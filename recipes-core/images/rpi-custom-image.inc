include recipes-core/images/core-image-minimal.bb

IMAGE_INSTALL += "  		\
	bash			\
	bash-completion 	\
	kernel-modules		\
	iw			\
	iptables		\
	linux-firmware-rpidistro-bcm43430 \
	libevent		\
	libusb1			\
	wpa-supplicant		\
	wifi-cfg-rpi		\
	wifi-rpi-connect	\
	"
