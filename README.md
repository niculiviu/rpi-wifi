## Project description
The project main purpose is related to video live streaming. The overall architecture is:

              ________________                   __________________    USB Ports            __
             | Vlc client     |                 |                  |-------------> USBs    |. |
             |     or         |  <---------->   | Raspberry Pi3B   |-------------> Video   |. |
             | other apps with|                 |                  |------------->
             | RTSP           |                 | (RTSP server)    |.............  Cameras |. |
             |________________|                 |__________________|------------->         |__|


## Description of the layer

This is the general hardware specific BSP overlay for the RaspberryPi device. The purpose of
customizations in this layer is to have a ready board for connection to an wifi network when boot complets,
and to start an application once a successful wifi connection is established. The application is called
UsbCamera-server and will handle the video live streaming.
This has been tested on raspberry Pi 3B(RevB with wifi BCM2837). The changes added to this layer are about having
a new customized rpi image, changes related to rpi boot mode(uboot and not the firmware that comes with the board),
rootfs packages and wifi services, scripts.

## Dependencies

This layer depends on:

* URI: git://git.yoctoproject.org/poky
  * branch: master
  * revision: HEAD

* URI: git://git.openembedded.org/meta-openembedded
  * layers: meta-oe, meta-multimedia, meta-networking, meta-python
  * branch: master
  * revision: HEAD

## Build

1. clone the repositories mentioned in the Dependencies
```
~$ mkdir rpi_wifi; cd rpi_wifi
~/rpi_wifi$ git clone git://git.yoctoproject.org/poky
~/rpi_wifi$ cd poky; git checkout -b thud origin/thud; cd ..;
~/rpi_wifi$ cd meta-openembedded; git checkout -b thud origin/thud; cd ..;
~/rpi_wifi$ git clone git://git.openembedded.org/meta-openembedded
~/rpi_wifi$ git clone git@gitlab.com:niculiviu/rpi-wifi.git
```
2. create building directory
```
rpi_wifi$ source poky/oe-init-build-env rpi64-build
```
3. add the neccessary layers to rpi64-build/conf/bblayers.conf and the dependencies above
```
BBLAYERS ?= " \
  /home/user/rpi_wifi/poky/meta \
  /home/user/rpi_wifi/poky/meta-poky \
  /home/user/rpi_wifi/poky/meta-yocto-bsp \
  /home/user/rpi_wifi/meta-openembedded/meta-oe \
  /home/user/rpi_wifi/meta-openembedded/meta-networking \
  /home/user/rpi_wifi/meta-openembedded/meta-python \
  /home/user/rpi_wifi/rpi-wifi \
  "
```
4. Set MACHINE in local.conf and enable uart
```
MACHINE = "raspberrypi3-64"
enable_uart = "1"
```
5. build development-image
```
rpi64-build$ bitbake rpi-custom-image-development
```
6. flashing image to sdcard
```
cd ~/rpi_wifi/rpi64-build/tmp/deploy/images/raspberrypi3-64
sudo dd if=rpi-custom-image-development-raspberrypi3-64-20190808115510.rootfs.rpi-sdimg of=<device> bs=512 conv=sync
```
7. To connect to your wifi network just add a file called wifi.cfg int the boot partition.
```
# contents of the wifi.cfg
wifiNetwork=Test
password=1234
```
8. Boot your rpi

