SUMMARY = "U-Boot bootloader tools"

do_install_append() {
    install -d ${D}${bindir}
    install -m 0755 tools/mkenvimage ${D}${bindir}/uboot-mkenvimage
    ln -sf uboot-mkenvimage ${D}${bindir}/mkenvimage
}




ALLOW_EMPTY_${PN} = "1"
FILES_${PN}-mkenvimage = "${bindir}/uboot-mkenvimage ${bindir}/mkenvimage"
