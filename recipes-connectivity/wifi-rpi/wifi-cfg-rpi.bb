SUMMARY 	= "Systemd for wifi configuration"
DESCRIPTION = "This service will check if there is a file called wifi.cfg into boot partiton. If it does then \
it will copy it to /etc/"
MAINTAINER	= "Nicusor Huhulea <nicusor.huhulea@gmail.com>"
LICENSE 	= "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"
PV               = "0.1"

RDEPENDS_${PN} = "bash"

SRC_URI = "file://wifi-configure \
           file://wifi-cfg-rpi.service \
          "

DEPENDS = "systemd"

S = "${WORKDIR}"

inherit systemd

SYSTEMD_SERVICE_${PN} = "wifi-cfg-rpi.service"

do_install () {
        install -d ${D}/usr/bin
        install -d ${D}/etc/systemd/system

        install -m 700 ${S}/wifi-configure ${D}/usr/bin/
        install -m 644 ${S}/wifi-cfg-rpi.service ${D}/etc/systemd/system/
}

