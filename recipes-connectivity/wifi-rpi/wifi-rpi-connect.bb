SUMMARY     = "Systemd for wifi connection"
DESCRIPTION = "Service that will handle the wifi connection."
MAINTAINER  = "Nicusor Huhulea <nicusor.huhulea@gmail.com>"
LICENSE     = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"
PV               = "0.1"

RDEPENDS_${PN} = "bash"

SRC_URI = "file://wifi-connect \
           file://wifi-rpi-connect.service \
          "

DEPENDS = "systemd"

S = "${WORKDIR}"

inherit systemd

SYSTEMD_SERVICE_${PN} = "wifi-rpi-connect.service"

do_install () {
        install -d ${D}/usr/bin
        install -d ${D}/etc/systemd/system

        install -m 700 ${S}/wifi-connect ${D}/usr/bin/
        install -m 644 ${S}/wifi-rpi-connect.service ${D}/etc/systemd/system/
}
