LINUX_VERSION ?= "4.19.50"

SRCREV = "2f8d963db206ce596f9a9e951ec425e9c3e1b4d9"
SRC_URI = " \
    git://github.com/raspberrypi/linux.git;branch=rpi-4.19.y \
	"

require linux-raspberrypi.inc
