SUMMARY 	= "Usb camera server"
DESCRIPTION = ""
HOMEPAGE 	= ""
MAINTAINER  = "Nicusor Huhulea <nicusor.huhulea@gmail.com>"
LICENSE     = "GPLv2"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/GPL-2.0;md5=801f80980d171dd6425610833a22dbe6"
PV          = "0.1"

DEPENDS = "libusb1 libevent"
SRCREV  = "0d273104bb1059230c0dc771e412bef8d23d8d41"

SRC_URI = "git:///home/nicu/work/raspberryPi/usbCamera-server;protocol=file;branch=master \
           "

S = "${WORKDIR}/git"

do_compile() {
	cd ${S}
    oe_runmake
}

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${S}/build/bin/usbCamera-server ${D}${bindir}/usbCamera-server
}

TARGET_CC_ARCH += "${LDFLAGS}"

